﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Transactions;

namespace _4312
{
    public class Overload
    {
        
        static void Main(string[] args)
        {
            int currentfile = 1;
            while (currentfile < 101) 
            {
                StreamWriter sw = new StreamWriter(@$"A:\files\file{currentfile}.txt");
                for (int j = 0; j < 100000; j++)
                {
                    string firstpart = First_date();
                    string secondpart = Second_symbol_latin();
                    string thirdpart = Third_symbol_russian();
                    string fourthpart = Fourth_random_number();
                    string fifthpart = Fifth_random_float();
                    string resultingstring = $"{firstpart}||{secondpart}||{thirdpart}||{fourthpart}||{fifthpart}";
                    sw.WriteLine(resultingstring);
                }
                sw.Close();
                currentfile++;
            }
        }
        private static string First_date() 
        {
            DateTime startDate = new DateTime(2017, 1, 1);
            Random rnd = new Random();
            int rangeofdates = (DateTime.Today - startDate).Days;
            DateTime myTime = startDate.AddDays(rnd.Next(rangeofdates));
            return myTime.ToShortDateString();
        }
        private static string Second_symbol_latin() 
        {
            char[] symbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            Random r = new Random();
            string finalresult = "";
            for (int i = 0; i < 10; i++)
            {
                finalresult += symbols[r.Next(0, 52)].ToString();
            }
            return finalresult;
        }
        private static string Third_symbol_russian() 
        {
            char[] symbols = "аАбБвВгГдДеЕёЁжЖзЗиИйЙкКлЛмМнНоОпПрРсСтТуУфФхХчЧцЦшШщЩъЪыЫьЬэЭюЮяЯ".ToCharArray();
            Random r = new Random();
            string finalresult = "";
            for (int i = 0; i < 10; i++)
            {
                finalresult += symbols[r.Next(0, 66)].ToString();
            }
            return finalresult;
        }
        private static string Fourth_random_number() 
        {
            Random r = new Random();
            int value = r.Next(0, 1000000);
            int finalresult = value;
            return finalresult.ToString();
        }
        private static string Fifth_random_float() 
        {
            Random r = new Random();
            double value = r.Next(100000000, 2000000000);
            double finalresult = value / 100000000;
            return finalresult.ToString();
        }
    }
}
